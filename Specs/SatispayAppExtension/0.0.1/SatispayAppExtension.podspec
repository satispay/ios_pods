Pod::Spec.new do |s|
  s.name             = 'SatispayAppExtension'
  s.version          = '0.0.1'
  s.summary          = 'Satispay App Extension support library'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
    Support library for the Satispay App Extension
                       DESC

  s.homepage         = 'https://bitbucket.org/despescials/satispay-app-extension/overview'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Pierluigi D\'Andrea' => 'pierluigi.dandrea@satispay.com' }
  s.source           = { :git => 'https://bitbucket.org/despescials/satispay-app-extension.git', :tag => s.version.to_s }

  s.ios.deployment_target = '8.0'

  s.source_files = 'SatispayAppExtension/Classes/**/*'
  s.public_header_files = 'SatispayAppExtension/Classes/SatispayExtension.h'

end
