Pod::Spec.new do |s|
  s.name             = 'SatispayAppExtension'
  s.version          = '0.0.5'
  s.summary          = 'Satispay App Extension support library'
  s.description      = <<-DESC
    Support library for the Satispay App Extension
                       DESC

  s.homepage         = 'https://bitbucket.org/despescials/satispay-app-extension/overview'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Pierluigi D\'Andrea' => 'pierluigi.dandrea@satispay.com' }
  s.source           = { :git => 'https://bitbucket.org/despescials/satispay-app-extension.git', :tag => s.version.to_s }

  s.ios.deployment_target = '8.0'

  s.source_files = 'SatispayAppExtension/Classes/**/*'
  s.public_header_files = 'SatispayAppExtension/Classes/SatispayExtension.h'

end
